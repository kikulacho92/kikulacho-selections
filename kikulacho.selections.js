function UnsupportedBrowserException() {
	this.code = "UnsupportedBrowserException";
	this.message = "It is currently impossible to get selected text in your browser";
}

getSelectedText = function() {
	if (window.getSelection) {
		return window.getSelection();
	}
	if (document.getSelection) {
		return document.getSelection();
	}
	else {
		throw new UnsupportedBrowserException();
	}
}

function getIndexAsChildNode(node) {
	parent = node.parentNode;
	index = 0;
	try {
		while (index < parent.childNodes.length) {
			if (parent.childNodes[index] === node) {
				return index;
			}
			index++;
		}
		return -1;
	} catch (e) {
		console.log("Error in getIndexAsChildNode with node = ", node, "\nparent = ", node.parentNode);
		throw e;
	}
}
function createRange(range, highestParent) {
//	console.clear();
	if (range.commonAncestorContainer.nodeType == 3) {
		return new TextContainer(range, highestParent);
	} else {
		return new NodeContainer(range, highestParent);
	}
}

function SplitParentError(message) {
	this.code = "SplitParentError";
	this.message = message;
}

function splitParent(node1, node2) {
	let parent = node1.parentNode;
	if (parent !== node2.parentNode) {
		throw new SplitParentError("The two nodes passed as arguments ar not siblings");
	}

	let index1 = getIndexAsChildNode(node1);
	let index2 = getIndexAsChildNode(node2);

	if (index1 >= index2) {
		throw new SplitParentError("The first node must come before the second one");
	}

	let newNodes = [];

	let parent1 = parent.cloneNode(true);
	let i = parent.childNodes.length - 1;
	while (i > index1) {
		parent1.childNodes[i--].remove();
	}
	newNodes.push(parent1);

	i = index1 + 1;
	while (i < index2) {
		newNodes.push(parent.childNodes[i++].cloneNode(true));
	}

	let parent2 = parent.cloneNode(true);
	i = index2 - 1;
	while (i >= 0) {
		parent2.childNodes[i--].remove();
	}
	newNodes.push(parent2);

	let indexOfParent = getIndexAsChildNode(parent);
	index1 = indexOfParent;
	index2 = indexOfParent + newNodes.length + 1;
	newParent = parent.parentNode;
	parent.replaceWith(...newNodes);
	return [parent1, parent2];
}

function splitParentRecursively(highestParent, node1, node2, range) {
	let finalNode = highestParent.parentNode;
	let retArray = splitParent(node1, node2);
	while (retArray[0].parentNode && finalNode !== retArray[0].parentNode) {
		retArray = splitParent(...retArray);
	}
	range.setStart(retArray[0].nextSibling, 0);
	if (retArray[0].nextSibling.nodeType == 3) {
		range.setEnd(retArray[0].nextSibling, retArray[0].nextSibling.nodeValue.length);
	} else {
		range.setEndBefore(retArray[1]);
	}
	return retArray;
}

function NonSettableError() {
	this.code = "NonSettableError";
	this.message = "This variable is not settable";
}

function getParentOfTag(node, tag, highestParent) {
	let parent = node.parentNode;
	while (parent != null && parent !== highestParent) {
		if (parent.tagName.toLowerCase() == tag.toLowerCase()) {
			return parent;
		}
		parent = parent.parentNode;
	}
	return null;
}


function getIndexOnceNormalized(textNode) {
	indexOnceNormalized = 0;
	child = textNode.parentNode.firstChild;
	while (child !== textNode) {
		if (child.nodeType == 3) {
			if ((child.previousSibling != null) && (child.previousSibling.nodeType !== 3)) {
				indexOnceNormalized += 1;
			}
		} else {
			indexOnceNormalized += 1;
		}
		child = child.nextSibling;
	}
	if (textNode.nodeType == 3 && textNode.previousSibling && textNode.previousSibling.nodeType !== 3) {
		indexOnceNormalized += 1;
	}
	return indexOnceNormalized;
}

function TextContainer(myrange, highestParent) {

	this.range = myrange;
	this.highestParent = highestParent;
	Object.defineProperties(this,
		{"ancestor": {
						"get": function() {return this.range.commonAncestorContainer},
						"set": function() {throw new NonSettableError}
					 }
		}
	);
	this.containerIndex = getIndexAsChildNode(this.ancestor);
	this.previousText = this.ancestor.nodeValue.substring(0, this.range.startOffset);
	this.middleText = this.ancestor.nodeValue.substring(this.range.startOffset, this.range.endOffset);
	this.followingText = this.ancestor.nodeValue.substring(this.range.endOffset);

	this.getParentOfTag = function(tag) {
		return getParentOfTag(this.ancestor, tag, this.highestParent);
	}

	this.isInTag = function(tag) {
		let parent = this.getParentOfTag(tag);

		if (parent != null) {
			return true;
		}
		return false
	}

	this.showRange = function() {
		getSelection().removeAllRanges();
		getSelection().addRange(this.range);
	}

	this.addTag = function(tag, attributes = {}) {
		console.log("TextContainer.addTag");

		console.log(this.previousText);
		console.log(this.middleText);
		console.log(this.followingText);

		if (this.isInTag(tag)) {
			return;
		}

		if (this.range.collapsed) {
			return;
		}

		let previousNode = document.createTextNode(this.previousText);
		let middleNode = document.createElement(tag);
		for (key in attributes) {
			middleNode.setAttribute(key, attributes[key])
		}
		middleNode.innerHTML = this.middleText;
		let followingNode = document.createTextNode(this.followingText);
		//I store the parent node of ancestor in parent because once I set the innerHTML this.ancestor will be out of the DOM
		let parent = this.ancestor.parentNode;
		parent.childNodes[this.containerIndex].replaceWith(previousNode, middleNode, followingNode);

		this.range = new Range();
		this.range.setStart(middleNode.firstChild, 0);
		this.range.setEnd(middleNode.firstChild, middleNode.firstChild.nodeValue.length);
		this.showRange();
	}

	this.removeTag = function(tag) {
		if (!this.isInTag(tag)) {
			return;
		}

		if (this.range.collapsed) {
			return;
		}

		let nodesArray = [];
		if (this.previousText.length > 0) {
			let previousNode = document.createElement(tag);
			previousNode.innerText = this.previousText;
			nodesArray.push(previousNode);
		}
		let middleNode = document.createTextNode(this.middleText);
		nodesArray.push(middleNode);

		if (this.followingText.length > 0) {
			let followingNode = document.createElement(tag);
			followingNode.innerText = this.followingText;
			nodesArray.push(followingNode);
		}

		this.ancestor.parentNode.replaceWith(...nodesArray);

		let newStartOffset;

		if (middleNode.previousSibling && middleNode.previousSibling.nodeType == 3) {
			newStartOffset = middleNode.previousSibling.nodeValue.length;
		} else {
			newStartOffset = 0;
		}
		let newEndOffset = newStartOffset + this.middleText.length;

		let newContainer = middleNode.parentNode;
		let indexOnceNormalized = 0;
		let child = newContainer.firstChild;
		while (child !== middleNode) {
			if (child.nodeType == 3) {
				if ((child.previousSibling != null) && (child.previousSibling.nodeType !== 3)) {
					indexOnceNormalized += 1;
				}
			} else {
				indexOnceNormalized += 1;
			}
			child = child.nextSibling;
		}

		newContainer.normalize();
		this.range.setStart(newContainer.childNodes[indexOnceNormalized], newStartOffset);
		this.range.setEnd(newContainer.childNodes[indexOnceNormalized], newEndOffset);
		this.showRange();

		let ancestorOfTag = this.getParentOfTag(tag);

		let splitNode1 = newContainer.childNodes[indexOnceNormalized-1];
		let splitNode2 = newContainer.childNodes[indexOnceNormalized+1];
		while (ancestorOfTag != null) {
			newSplit = splitParentRecursively(
				ancestorOfTag,
				splitNode1,
				splitNode2,
				this.range
			)
			if (this.ancestor === this.highestParent) {
				break;
			}
			ancestorOfTag = this.getParentOfTag(tag);
			splitNode1 = newSplit[0];
			splitNode2 = newSplit[1];
		}
	}
}

function unwrapTag(node, tag) {
		let i = node.children.length - 1;
		while (i >= 0) {
			if (node.children.length > 0) {
				try {
				let thisChild = node.children[i];
				if (thisChild.tagName.toLowerCase() == tag.toLowerCase()) {
					while (thisChild.firstChild) {
						node.insertBefore(thisChild.firstChild, thisChild);
					}
				thisChild.remove();
				} else {
					unwrapTag(node.children[i], tag);
					i--;
				}
				} catch (e) {
					console.log("Error in unwrapTag");
					console.log('node: ', node);
					console.log('node.children: ', node.children);
					console.log('i: ', i);
					console.log('node.children[i]: ', node.children[i]);
					throw e;
				}
			}
			else {
				if (node.childNodes.length == 0 && node.tagName == tag) {
					node.remove();
				}
				i--;
			}
		}
}

function NodeContainer(myrange, highestParent) {

	this.range = myrange;
	this.highestParent = highestParent;
	Object.defineProperties(this,
		{"ancestor": {
						"get": function() {return this.range.commonAncestorContainer},
						"set": function() {throw new NonSettableError}
					 }
		}
	);

	this.getParentOfTag = function(tag) {
		return getParentOfTag(this.ancestor, tag, this.highestParent);
	}

	this.isInTag = function(tag) {
		if (this.ancestor == this.highestParent) {
			return false;
		}
		let isWrappedInTag = this.isWrappedInTag(tag);
		let allTextNodesAreWrappedInTag = this.allTextNodesAreWrappedInTag(tag);
//		console.log('isWrappedInTag: ', isWrappedInTag, 'allTextNodesAreWrappedInTag: ', allTextNodesAreWrappedInTag);
		return isWrappedInTag || allTextNodesAreWrappedInTag;
	}

	this.isWrappedInTag = function(tag) {
		if (this.ancestor.tagName.toLowerCase() == tag.toLowerCase()) {
			return true;
		}
		let parent = this.getParentOfTag(tag);
//		console.log('this.getParentOfTag: ', parent);
		return parent != null;
	}

	this.allTextNodesAreWrappedInTag = function(tag) {
		let selectionNode = document.createElement('span');
		selectionNode.append(this.range.cloneContents());
		let textNodes = [];
//		console.log(selectionNode.childNodes);
		for (i=0;i<selectionNode.childNodes.length;i++) {
			if (selectionNode.childNodes[i].nodeType == 3) {
				textNodes.push(selectionNode.childNodes[i]);
			}
		}

		let descendants = Array.from(selectionNode.getElementsByTagName('*'));
		descendants = descendants.filter(element => (element.childNodes.length != element.children.length));

		for (i=0;i<descendants.length;i++) {
			for (j=0; j<descendants[i].childNodes.length;j++) {
				if (descendants[i].childNodes[j].nodeType == 3) {
					textNodes.push(descendants[i].childNodes[j]);
				}
			}
		}

		let isAnyTextNotWrapped = textNodes.some(
			function(element) {
				return getParentOfTag(element, tag, this.highestParent) == null;
			}
		)

		return !isAnyTextNotWrapped;
	}

	this.showRange = function() {
		getSelection().removeAllRanges();
		getSelection().addRange(this.range);
	}

	this.removeTag = function(tag) {
		let isInTag = this.isInTag(tag);
		console.log("isInTag:", isInTag);
		if (!isInTag) {
			return;
		}

		console.log("this.ancestor:", this.ancestor);

		let previousRange = new Range();
		previousRange.setStart(this.ancestor, 0);
		previousRange.setEnd(this.range.startContainer, this.range.startOffset);
		let followingRange = new Range();
		followingRange.setStart(this.range.endContainer, this.range.endOffset);
		followingRange.setEnd(this.ancestor, this.ancestor.childNodes.length);

		let newNodes = [];
		let previousNode;
		previousNode = document.createElement(tag);
		previousNode.append(previousRange.cloneContents());
		if (previousNode.childNodes.length > 0) {
			newNodes.push(previousNode);
		}
		console.log("previousNode:", previousNode);

		let selectionContents = document.createElement('span');
		selectionContents.append(this.range.cloneContents());
		unwrapTag(selectionContents, tag);

		while (selectionContents.childNodes.length > 0) {
			newNodes.push(selectionContents.firstChild);
			selectionContents.firstChild.remove();
		}

		let followingNode;
		followingNode = document.createElement(tag);
		followingNode.append(followingRange.cloneContents());
		if (followingNode.childNodes.length > 0) {
			newNodes.push(followingNode);
		}
		console.log('newNodes:', newNodes);

		if (this.ancestor.tagName.toLowerCase() == tag.toLowerCase()) {
			if (this.ancestor.attributes.length == 0) {
				this.ancestor.replaceWith(...newNodes);
			} else {
				newContainer = document.createElement('span');
				for (attribute of this.ancestor.attributes) {
					newContainer.setAttribute(attribute.name, attribute.value);
				}
				for (node of newNodes) {
					newContainer.append(node);
				}
				this.ancestor.replaceWith(newContainer);
			}
		} else {
			while (this.ancestor.firstChild) {
				this.ancestor.lastChild.remove();
			}
			for (node of newNodes) {
				this.ancestor.append(node);
			}
		}

		let startIndex, startOffset;
		if (previousNode.childNodes.length > 0) {
			startIndex = 1;
			startOffset = 0;
		} else {
			startIndex = getIndexOnceNormalized(newNodes[0]);
			if (newNodes[0].previousSibling.nodeType == 3) {
				startOffset = newNodes[0].previousSibling.nodeValue.length;
			}
			else {
				startOffset = 0;
			}
		}


		if (followingNode.childNodes.length > 0) {
			endIndex = getIndexOnceNormalized(newNodes[followingNode]);
			endOffset = 0;
		} else {
			endIndex = getIndexOnceNormalized(newNodes[newNodes.length - 1]);
			endOffset = newNodes[newNodes.length - 1].nodeType !== 3 ? newNodes[newNodes.length - 1].childNodes.length : newNodes[newNodes.length - 1].nodeValue.length;
		}
		let newContainer = newNodes[0].parentNode;
		newNodes[0].parentNode.normalize();
		this.range.setStart(newContainer.childNodes[startIndex], startOffset);
		this.range.setEnd(newContainer.childNodes[endIndex], endOffset);

		if (this.ancestor == this.highestParent) {
			return;
		}
		let ancestorOfTag = this.getParentOfTag(tag);

		let splitNode1 = previousNode;
		let splitNode2 = followingNode;
		while (ancestorOfTag != null) {
			newSplit = splitParentRecursively(
				ancestorOfTag,
				splitNode1,
				splitNode2,
				this.range
			)
			if (this.ancestor === this.highestParent) {
				break;
			}
			ancestorOfTag = this.getParentOfTag(tag);
			splitNode1 = newSplit[0];
			splitNode2 = newSplit[1];
		}

		let emptyTags = document.getElementsByTagName('a');
		for (emptyTag of emptyTags) {
			if (emptyTag.childNodes.length == 0) {
				emptyTag.remove();
			}
		}
	}

	this.addTag = function(tag, attributes={}) {
		let isInTag = this.isInTag(tag)
		console.log("isInTag: ", isInTag);
		if (isInTag) {
			return;
		}

		let previousRange = new Range();
		previousRange.setStart(this.ancestor, 0);
		previousRange.setEnd(this.range.startContainer, this.range.startOffset);
		let followingRange = new Range();
		followingRange.setStart(this.range.endContainer, this.range.endOffset);
		followingRange.setEnd(this.ancestor, this.ancestor.childNodes.length);

		let newNode = this.ancestor.cloneNode();
		if (!previousRange.collapsed) {
			newNode.append(previousRange.cloneContents());
		}
		let newTaggedElement = document.createElement(tag);
		for (key in attributes) {
				newTaggedElement.setAttribute(key, attributes[key])
		}

		let selectionContents = document.createElement('span');
		selectionContents.append(this.range.cloneContents());
		unwrapTag(selectionContents, tag);
		while (selectionContents.childNodes.length > 0) {
			newTaggedElement.append(selectionContents.firstChild);
		}
		newNode.append(newTaggedElement);
		if (!followingRange.collapsed) {
			newNode.append(followingRange.cloneContents());
		}
		this.ancestor.replaceWith(newNode);
		this.range.setStart(newTaggedElement, 0);
		this.range.setEnd(newTaggedElement, newTaggedElement.childNodes.length);
		this.showRange();
	}
}

getRanges = function(filterFunction = null, highestParent = document.body) {
	selection = getSelectedText();
	ranges = [];
	for (i=0;i< selection.rangeCount;i++) {
		ranges.push(selection.getRangeAt(i));
	}

	if (filterFunction && typeof filterFunction === 'function') {
		ranges = ranges.filter(filterFunction);
	}

	for (i=0;i< ranges.length;i++) {
		if (highestParent && typeof highestParent === 'function') {
			ranges[i] = createRange(ranges[i], highestParent(ranges[i]));
		} else {
			ranges[i] = createRange(ranges[i], highestParent);
		}
	}
	return ranges
}


function ToggleButton(button) {
	this.element = button;
	this.element.style.border = 'outset';
	this.tag = button.getAttribute('tag');
	this.attributes = {};
	if (button.hasAttribute('attributes')) {
		let attributes = button.getAttribute('attributes');
		attributes = attributes.split(';');
		for (attribute of attributes) {
			let pair = attribute.split('=');
			if (pair.length != 2 || pair[0] == '' || pair[1] == '') {
				throw Exception("The attribute " + pair + "is not correctly formed.");
			}
			let key = pair[0].trim();
			let value = pair[1].trim();
			this.attributes[key] = value;
		}
	}
	this._ranges = [];
	Object.defineProperties(this,
		{"ranges": {
						"get": function() {return this._ranges},
						"set": function(array) {this._ranges = array}
					 }
		}
	);
	this.rangesCount = this.ranges.length;

	this.addTag = function(ranges, tag, attributes) {
		for (i=0; i<ranges.length; i++) {
			this.ranges[i].addTag(tag, attributes);
		}
	}
	this.removeTag = function(event) {
		for (i=0; i<this.ranges.length; i++) {
			this.ranges[i].removeTag(this.tag);
		}
	}
	this.element.onclick = () => {this.addTag(this.ranges, this.tag, this.attributes)};

	this.isToRemove = function() {
		return this.ranges.every(
			function(element, index) {
				return element.isInTag(this.tag);
			},
			this
		)
	}

	this.setAsRemover = function() {
		this.element.style.border = 'inset';
		this.element.onclick = () => {this.removeTag(this.ranges, this.tag)};
	}

	this.setAsAdder = function() {
		this.element.style.border = 'outset';
		this.element.onclick = () => {this.addTag(this.ranges, this.tag, this.attributes)};
	}

	this.updateStatus = function() {
		if (this.isToRemove()) {
			this.setAsRemover();
		} else {
			this.setAsAdder();
		}
	}
}

var toggleButtons = Array.from(document.getElementsByClassName("kikulacho.selections.togglebutton")).map(x => new ToggleButton(x));

document.addEventListener('mouseup',
	function(event) {
		let ranges = getRanges(
			filterFunction = kikulacho.selections.configuration.filterFunction,
			highestParent = document.highestParent
		);
		for (button of toggleButtons) {
			button.ranges = ranges;
			button.updateStatus(ranges);
		}
	}
);
